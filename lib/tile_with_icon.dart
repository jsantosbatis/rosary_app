import 'package:flutter/material.dart';

class TileWithIcon extends StatefulWidget {
  TileWithIcon({Key key, this.title, this.icon, this.onTap}) : super(key: key);

  final String title;
  final String icon;
  final Function onTap;
  @override
  _TileWithIconState createState() =>
      _TileWithIconState(this.title, this.icon, this.onTap);
}

class _TileWithIconState extends State<TileWithIcon> {
  _TileWithIconState(this.title, this.icon, this.onTap);
  final String title;
  final String icon;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          this.onTap(context);
        },
        child: Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.only(bottom: 20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Color(0xFFD9E2EC),
                      blurRadius: 5.0,
                      offset: Offset(0, 5)),
                ]),
            child: Row(
              children: [
                Image(
                  image: AssetImage(icon),
                  height: 65,
                  width: 65,
                ),
                Expanded(
                    child: Text(
                  this.title,
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                )),
              ],
            )));
  }
}