class Mystery {
  String title;
  String lecture;
  String meditationText;
  String image;
  int order;

  Mystery.fromJson(dynamic jsonData) {
    this.title = jsonData["title"];
    this.lecture = jsonData["lecture"];
    this.meditationText = jsonData["meditationText"];
    this.image = jsonData["image"];
    this.order = jsonData["order"];
  }

  static List<Mystery> toMysteryList(List<dynamic> list) {
      List<Mystery> mysteryList = List<Mystery>();
      list.forEach((jsonData) {
        mysteryList.add(Mystery.fromJson(jsonData));
      });
      return mysteryList;
  }
}