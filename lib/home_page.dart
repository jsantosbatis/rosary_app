import 'package:flutter/material.dart';
import 'package:hello_world/chapelet_page.dart';
import 'package:hello_world/mystery_list_page.dart';
import 'package:hello_world/tile_with_icon.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Accueil"),
      ),
      body: Container(
          margin: EdgeInsets.only(left: 20, right: 20, top: 20),
          child: IntrinsicHeight(
              child: Column(
            children: <Widget>[
              TileWithIcon(
                title: "Prier le chapelet",
                icon: "assets/images/icon/priere_1.png",
                onTap: (context) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ChapeletPage()));
                },
              ),
              TileWithIcon(
                  title: "Les Mystères",
                  icon: "assets/images/icon/vitrail.png",
                  onTap: (context) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MysteryListPage()));
                  }),
            ],
          ))),
      backgroundColor: Color(0XFFF0F0F0),
    );
  }
}
