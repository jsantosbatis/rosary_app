import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hello_world/business/mystery.dart';
import 'package:hello_world/mystery_tile.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ChapeletStep { CREDO, MARIE, FATHER, GLORIA, MYSTERY, FATIMA }

class ChapeletPage extends StatefulWidget {
  ChapeletPage({Key key}) : super(key: key);
  @override
  _ChapeletPageState createState() => _ChapeletPageState();
}

class _ChapeletPageState extends State<ChapeletPage> {
  _ChapeletPageState();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = true;
  String _title = "Prier le Chapelet";
  SharedPreferences _sharedPref;
  List<ChapeletStep> _steps;
  List<Mystery> _mysteryList;
  int _stepIndex = 0;
  int _stepMarie = 0;
  int _stepMystery = -1;
  String _credoHeader;
  String _credoPray;
  String _marieHeader;
  String _mariePray;
  String _fatherHeader;
  String _fatherPray;
  String _gloriaHeader;
  String _gloriaPray;
  String _fatimaHeader;
  String _fatimaPray;
  bool _recoveryRosaryShowed;

  @override
  void initState() {
    super.initState();
    List<ChapeletStep> steps = List<ChapeletStep>();
    steps.add(ChapeletStep.CREDO);
    steps.add(ChapeletStep.FATHER);
    steps.add(ChapeletStep.MARIE);
    steps.add(ChapeletStep.MARIE);
    steps.add(ChapeletStep.MARIE);
    steps.add(ChapeletStep.GLORIA);
    for (var i = 0; i < 5; i++) {
      steps.add(ChapeletStep.MYSTERY);
      steps.add(ChapeletStep.FATHER);
      for (var j = 0; j < 10; j++) {
        steps.add(ChapeletStep.MARIE);
      }
      steps.add(ChapeletStep.GLORIA);
      steps.add(ChapeletStep.FATIMA);
    }
    this._fetchPray().then((pray) {
      this._fetchMystery().then((mysteryList) {
        this._initSharedPreferences();
        setState(() {
          _isLoading = false;
          _steps = steps;
          _gloriaHeader = pray["gloria_header"];
          _gloriaPray = pray["gloria_text"];
          _fatherHeader = pray["father_header"];
          _fatherPray = pray["father_text"];
          _credoHeader = pray["credo_header"];
          _credoPray = pray["credo_text"];
          _marieHeader = pray["marie_header"];
          _mariePray = pray["marie_text"];
          _fatimaHeader = pray["fatima_header"];
          _fatimaPray = pray["fatima_text"];
          _mysteryList = mysteryList;
        });
      });
    });
  }

  void _initSharedPreferences() async {
    _sharedPref = await SharedPreferences.getInstance();
    setState(() {
      try {
        _stepIndex = _sharedPref.getInt("stepIndex") ?? _stepIndex;
        _stepMarie = _sharedPref.getInt("stepMarie") ?? _stepMarie;
        _stepMystery = _sharedPref.getInt("stepMystery") ?? _stepMystery;
        if (_stepIndex != 0) {
          _recoveryRosaryShowed = false;
        } else {
          _recoveryRosaryShowed = true;
        }
      } on Exception {
        developer.log("exception");
      }
    });
  }

  Future<List<Mystery>> _fetchMystery() async {
    String jsonPath = "";
    List<Mystery> items = List<Mystery>();
    switch (DateTime.now().weekday) {
      case 1:
      case 6:
        jsonPath = "assets/json/mystere_joyeux.json";
        break;
      case 2:
      case 5:
        jsonPath = "assets/json/mystere_douloureux.json";
        break;
      case 4:
        jsonPath = "assets/json/mystere_lumineux.json";
        break;
      case 3:
      case 7:
        jsonPath = "assets/json/mystere_glorieux.json";
        break;
      default:
    }
    await DefaultAssetBundle.of(context).loadString(jsonPath).then((onValue) {
      items = Mystery.toMysteryList(json.decode(onValue) as List<dynamic>);
    });
    return items;
  }

  Future<dynamic> _fetchPray() async {
    dynamic prayObject;
    String jsonPath = "assets/json/priere.json";
    await DefaultAssetBundle.of(context).loadString(jsonPath).then((onValue) {
      prayObject = json.decode(onValue);
    });
    return prayObject;
  }

  Widget getStartOfPray() {
    switch (this._steps[_stepIndex]) {
      case ChapeletStep.CREDO:
        return Text(
          _credoHeader,
          style: TextStyle(fontSize: 32),
        );
      case ChapeletStep.FATHER:
        return Text(
          _fatherHeader,
          style: TextStyle(fontSize: 32),
        );
      case ChapeletStep.MARIE:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Container(
                margin: EdgeInsets.only(bottom: 20),
                width: 50,
                height: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.teal,
                ),
                child: Text("$_stepMarie",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.w500)),
              ),
            ),
            Text(
              _marieHeader,
              style: TextStyle(fontSize: 32),
            ),
          ],
        );
      case ChapeletStep.GLORIA:
        return Text(
          _gloriaHeader,
          style: TextStyle(fontSize: 32),
        );
      case ChapeletStep.FATIMA:
        return Text(
          _fatimaHeader,
          style: TextStyle(fontSize: 32),
        );
      default:
        return null;
    }
  }

  Widget getWidgetPray() {
    switch (this._steps[_stepIndex]) {
      case ChapeletStep.CREDO:
        return Text(this._credoPray, style: TextStyle(fontSize: 22));
      case ChapeletStep.FATHER:
        return Text(this._fatherPray, style: TextStyle(fontSize: 22));
      case ChapeletStep.MARIE:
        return Text(this._mariePray, style: TextStyle(fontSize: 22));
      case ChapeletStep.GLORIA:
        return Text(this._gloriaPray, style: TextStyle(fontSize: 22));
      case ChapeletStep.MYSTERY:
        Mystery data = _mysteryList[_stepMystery];
        return MysteryTile(
            title: data.title,
            lecture: data.lecture,
            text: data.meditationText,
            image: data.image,
            order: data.order);
      case ChapeletStep.FATIMA:
        return Text(this._fatimaPray, style: TextStyle(fontSize: 22));
      default:
        return Text("Not found");
    }
  }

  void onNext() {
    if (_stepIndex < _steps.length - 1) {
      setState(() {
        _stepIndex++;
        if (_steps[_stepIndex] == ChapeletStep.MARIE) {
          _stepMarie++;
        } else {
          _stepMarie = 0;
        }
        if (_steps[_stepIndex] == ChapeletStep.MYSTERY) {
          _stepMystery++;
        }
        _sharedPref.setInt("stepIndex", _stepIndex);
        _sharedPref.setInt("stepMarie", _stepMarie);
        _sharedPref.setInt("stepMystery", _stepMystery);
      });
    } else {
      _sharedPref.clear().whenComplete(() => Navigator.pop(context));
    }
  }

  void resetRosary() {
    _sharedPref.clear().whenComplete(() {
      setState(() {
        _stepIndex = 0;
        _stepMarie = 0;
        _stepMystery = -1;
      });
    });
  }

  void _showRecoveryMessage() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Reprise du chapelet"),
      duration: Duration(seconds: 2),
    ));
    setState(() {
      _recoveryRosaryShowed = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: Text(_title),
          ),
          body: Center(
            child: new CircularProgressIndicator(),
          ));
    }
    if (!_recoveryRosaryShowed) {
      _showRecoveryMessage();
    }
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(_title),
        actions: [
          IconButton(
            icon: Icon(Icons.replay),
            onPressed: () => resetRosary(),
          )
        ],
      ),
      body: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(20),
              child: Center(child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(child: getStartOfPray()),
                  Container(child: getWidgetPray()),
                ],
              )))),
      backgroundColor: Color(0XFFF0F0F0),
      floatingActionButton: FloatingActionButton(
        onPressed: this.onNext,
        child: Icon(Icons.arrow_forward),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
