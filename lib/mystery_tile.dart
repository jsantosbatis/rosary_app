import 'package:flutter/material.dart';

class MysteryTile extends StatefulWidget {
  MysteryTile(
      {Key key, this.title, this.lecture, this.text, this.image, this.order})
      : super(key: key);

  final String title;
  final String lecture;
  final String text;
  final String image;
  final int order;
  @override
  State<StatefulWidget> createState() => _MysteryTileState(
      title: this.title,
      lecture: this.lecture,
      text: this.text,
      image: this.image,
      order: this.order);
}

class _MysteryTileState extends State<MysteryTile> {
  _MysteryTileState(
      {this.title, this.lecture, this.text, this.image, this.order});
  final String title;
  final String lecture;
  final String text;
  final String image;
  final int order;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFD9E2EC), blurRadius: 5.0, offset: Offset(0, 5))
          ],
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Center(child: Image(image: AssetImage(this.image))),
              margin: EdgeInsets.only(bottom: 10),
            ),
            Text(
              this.title,
              style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            ),
            Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
                child: Text(this.lecture,
                    style:
                        TextStyle(fontSize: 22, fontStyle: FontStyle.italic))),
            Text(
              this.text,
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
            )
          ],
        ));
  }
}
