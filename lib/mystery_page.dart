import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:hello_world/business/mystery.dart';

import 'mystery_tile.dart';

class MysteryPage extends StatefulWidget {
  MysteryPage({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _MysteryPageState createState() => _MysteryPageState(title: this.title);
}

class _MysteryPageState extends State<MysteryPage> {
  _MysteryPageState({this.title});
  final String title;
  List<Mystery> _mysteryList = List<Mystery>();

  Future<List<Mystery>> fetchMystery() async {
    List<Mystery> items = List<Mystery>();
    String jsonPath = "";
    switch (this.title) {
      case "Joyeux":
        jsonPath = "assets/json/mystere_joyeux.json";
        break;
      case "Douloureux":
        jsonPath = "assets/json/mystere_douloureux.json";
        break;
      case "Glorieux":
        jsonPath = "assets/json/mystere_glorieux.json";
        break;
      case "Lumineux":
        jsonPath = "assets/json/mystere_lumineux.json";
        break;
      default:
    }
    await DefaultAssetBundle.of(context).loadString(jsonPath).then((onValue) {
      items = Mystery.toMysteryList(json.decode(onValue) as List<dynamic>);
    });
    return items;
  }

  @override
  void initState() {
    super.initState();
    fetchMystery().then((data) {
      setState(() {
        _mysteryList = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Les Mystères ${widget.title}"),
      ),
      body: Container(
          child: ListView.builder(
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              itemCount: this._mysteryList.length,
              itemBuilder: (context, index) {
                Mystery data = this._mysteryList[index];
                return Container(
                    margin: EdgeInsets.only(top: 20),
                    child: MysteryTile(
                        title: data.title,
                        lecture: data.lecture,
                        text: data.meditationText,
                        image: data.image,
                        order: data.order));
              })),
      backgroundColor: Color(0XFFF0F0F0),
    );
  }
}
