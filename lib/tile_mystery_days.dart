import 'package:flutter/material.dart';
import 'package:hello_world/mystery_page.dart';
import 'dart:developer' as developer;

class TileMysteryDays extends StatefulWidget {
  TileMysteryDays({Key key, this.primaryText, this.days, this.color}) : super(key: key);

  final String primaryText;
  final List<int> days;
  final Color color;
  @override
  State<StatefulWidget> createState() => _TileMysteryDaysState(
      primaryText: this.primaryText, days: this.days, color: this.color);
}

class _TileMysteryDaysState extends State<TileMysteryDays> {
  _TileMysteryDaysState({this.primaryText, this.days, this.color});

  final String primaryText;
  final List<int> days;
  final Color color;

  bool isInRange() {
    DateTime today = DateTime.now();
    return this.days.contains(today.weekday);
  }

  String daysToString() {
    String daysString = "";
    String label = "";
    for (var i = 0; i < this.days.length; i++) {
      switch (this.days[i]) {
        case 1:
          label = "Lundi";
          break;
        case 2:
          label = "Mardi";
          break;
        case 3:
          label = "Mercredi";
          break;
        case 4:
          label = "Jeudi";
          break;
        case 5:
          label = "Vendredi";
          break;
        case 6:
          label = "Samedi";
          break;
        case 7:
          label = "Dimanche";
          break;
        default:
      }
      if (i != 0) {
        daysString += " - " + label;
      } else {
        daysString += label;
      }
    }
    return daysString;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MysteryPage(title: this.primaryText)));
        },
        child: Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.only(bottom: 15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
                gradient: LinearGradient(
                    begin: Alignment(-1.0, -1.0),
                    end: Alignment(1.0, -1.0),
                    stops: [0.05, 0.05], colors: [this.color, Colors.white]),
                boxShadow: [
                  BoxShadow(
                      color: Color(0xFFD9E2EC),
                      blurRadius: 5.0,
                      offset: Offset(0, 5))
                ]),
            child: Center(
                child: Column(
              children: <Widget>[
                Container(
                  child: Text(this.primaryText,
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.w500)),
                  margin: EdgeInsets.only(bottom: 10),
                ),
                Text(this.daysToString(), style: TextStyle(fontSize: 18))
              ],
            ))));
  }
}
