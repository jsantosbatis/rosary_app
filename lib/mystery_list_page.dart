import 'package:flutter/material.dart';
import 'package:hello_world/tile_mystery_days.dart';

class MysteryListPage extends StatefulWidget {
  MysteryListPage();
  final String title = "Les Mystères";
  @override
  State<MysteryListPage> createState() => _MysteryListPageState(this.title);
}

class _MysteryListPageState extends State<MysteryListPage> {
  _MysteryListPageState(this.title);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
            TileMysteryDays(primaryText: "Joyeux", days: [1, 6], color: Color(0XFF6bc5d2)),
            TileMysteryDays(
                primaryText: "Douloureux",
                days: [2, 5],
                color: Color(0XFFca5fa6)),
            TileMysteryDays(primaryText: "Lumineux", days: [4], color: Color(0XFFffdd67)),
            TileMysteryDays(
                primaryText: "Glorieux",
                days: [3, 7],
                color: Color(0XFFff8a5c)),
          ]),
      backgroundColor: Color(0XFFF0F0F0),
    );
  }
}
