import 'package:flutter/material.dart';
import 'package:hello_world/home_page.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rosaire',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: HomePage()//(title: 'Les mystères'),
    );
  }
}